# Email szolgáltatások

## Szempontok

Többféle célcsoportunk különböző, de kapcsolódó igényekkel rendelkezik:

* Legyen fenntartható, ne csődöljön be mostanában: például üzemeljen már egy ideje és legyen hihető üzleti modellje
* Minden elérhető legyen magyarul (ÁSZF, regisztráció, terméktámogatás, esetleg webmail)
* Magyar földön legyen üzemeltetve
* Magánszférát tiszteletben tartó, jogbiztonsággal rendelkező országban legyen üzemeltetve

## Magyarországon ingyen

* https://freemail.hu/ 10GB tárhely (Magyar Telekom Nyrt.)
* https://indamail.hu/ 2GB tárhely, POP3, IMAP (Indamedia csoport, volt VIPmail, Indexmail, DrótPostaGalamb)
* https://www.citromail.hu/ 2GB tárhely, max. 5MB/csatolmány (Sanoma Budapest Zrt.)
* https://www.euromail.hu/ 50MB (Humankraft Kft.)
* https://www.tvn.hu/reg/reg2.tvn (TVN.hu Kft.)

## Magyarországon fizetős

* https://www.mikrovps.net/hu/email-hosting/basic 15GB tárhely, 3000 Ft+áfa/év (0.89 EUR/hó) (MikroVPS Kft.)
* https://megacp.com/hosting.php?aid=urb40687&spt=1 Induló tárhely csomag, 500 MB tárhely, saját domén, korlátlan címek, 5600 Ft + ÁFA / 2 év (bruttó 300 Ft/hó) (3 in 1 Hosting Bt.)
* https://www.forpsi.hu/email/ bruttó 1905 Ft/év domén mellé, 1GB
* https://mailbox.hu/ 1 EUR/hó

## Magyarországon másodlagos email cím ingyen

Regisztrációhoz meglévő működő elektronikus levélcím szükséges, viszont mondjuk az elsődleges címhez képest további vagy jobb szolgáltatásokat biztosít.

* https://www.nethely.hu/ingyenes-tarhely
* https://atw.hu/ingyenes-webtarhely

## Etikus országban ingyen

* http://protonmail.com/ Svájc, csak webmail ingyenes, 500MB tárhely, 150 üzenet/nap
* https://tutanota.com/ Németország, csak webmail ingyenes, 1GB tárhely
* https://disroot.org/en/services/email Hollandia, nem alanyi jogon jár, regisztrációhoz átmenetileg szükséges egy elsődleges email fiók megadása

## Etikus országban másodlagos email cím ingyen

* https://webmail.vivaldi.net/ 5GB tárhely, max. 20MB/csatolmány, webmail, IMAPs, SMTPs, POP3s, 6 hónap után a weben inaktív fiókokat törlik, Norvégia (Vivaldi Technologies AS)

## Etikus országban fizetős

* https://mailbox.org/ 1 EUR/hó, 2 GB levelezőtárhelyet, 100 MB felhőtárhely, webmail, IMAP, WebDAV/CalDAV/CardDAV címtár és naptár, Open-Xchange, Németország (Heinlein Support GmbH)

## Külső összehasonlító oldalak

* https://www.privacytools.io/providers/email/
* https://en.wikipedia.org/wiki/Comparison_of_webmail_providers#General

## Pingback

Ha átnevezésre vagy áthelyezésre kerül ez a fájl, az összes közvetlen hivatkozást frissíteni kell:

* https://hup.hu/comment/2563434#comment-2563434
