# Zeneszolgáltatások

## Cél

* Új zenék megismerése
* Megoszthassuk közösségi hálónkkal, hogy mit hallgatunk vagy milyen stílusokat szeretünk
* Ideálisan régebbi eszközeinken is hallgathassuk illetve amennyiben nem vagyunk internet közelben vagy rossz a térerő
* A vállalati monopóliumi status quo decentralizálása a független alkotók, szabad tartalmak és szabad szoftverek irányába
  * https://en.wikipedia.org/wiki/Comparison_of_on-demand_music_streaming_services#Current_services
* Előnyben: fenntartható, ingyenes, nem követő, fizetős esetben DRM-mentes, esetleg FLOSS kliensből használható, közösségi médián mély hivatkozással megosztható (deep-linkelhető) alternatívák

## Fizetés nélkül nem teljes értékű

### Előfizetések

* YouTube.com, Music.YouTube.com: követ, a jövőben nyugtalanítóan halad a vendor lock-in irányába, a webes egyre több és tolakodóbb reklámért cserébe lehetővé teszi kiválasztott számok lejátszását is
* Spotify: csak regisztrációval hallgatható, mobilon csak rádió üzemmódot tud ingyen, a webes csak DRM bekapcsolásával használható, de egyre több és tolakodóbb reklámért cserébe lehetővé teszi kiválasztott számok lejátszását is
* https://music.amazon.com/ csak regisztrációval használható, ingyenes regisztrációval csak rádiózást tud
* https://Deezer.com/ regisztráció nélkül csak az első 30 másodperc hallgatható, ingyenes regisztrációval csak rádiózás tud
* https://listen.tidal.com/ fizetős regisztráció, anélkül csak az első 30 másodperc hallgatható
* https://music.apple.com/ fizetős regisztráció, anélkül csak az első 30 másodperc hallgatható

### Digitális lemezboltok

* https://en.wikipedia.org/wiki/Comparison_of_online_music_stores
* https://www.beatport.com/ vásárlás előtt enged regisztráció nélkül 2 percet belehallgatni
* https://www.pastemagazine.com/noisetrade/music/ reklámbejátszásmentes, a regisztráció ingyenes, anélkül csak az első 30 másodperc hallgatható, azzal akár le is tölthető vagy a készítők is támogathatóak
* http://www.8bitpeoples.com/ Letöltés csak regisztráció után, sokszor ingyen

## Szoftverek

### Szerver

* PeerTube
* https://funkwhale.audio/
* https://github.com/airsonic/airsonic
* https://github.com/sentriz/gonic

### Kliens

* mplayer
* VLC
* Clementine
* https://f-droid.org/en/packages/net.programmierecke.radiodroid2/

## Ajánlott szolgáltatók

* Hallgatás: regisztráció nélkül ingyen meghallgatható teljes egészében igény szerinti szám amire lehet mélyen is hivatkozni
* Letöltés: regisztráció nélkül ingyen letölthető teljes egészében igény szerinti szám amire lehet mélyen is hivatkozni
* Rádiózás: regisztráció nélkül ingyen hallgatható a tartalmak adott részhalmaza (csatornája, stílusa, lejátszólistája, stb) melyekre nekünk nincs finomabb ráhatásunk és mély hivatkozást sem tudunk megosztani amivel más belehallgathat a kiválasztott számokba

### Főleg szabad tartalmak

* Hallgatás/letöltés https://www.jamendo.com/ Hipstereknek különösen ajánlott, mert ez nagyon indie (Clementine és Libre.fm támogatás)
* Hallgatás/letöltés https://open.audio/
* Letöltés https://www.europeana.eu/en/search?page=1&qf=TYPE%3A%22SOUND%22&query=RIGHTS%3A*creative*
* Hallgatás/letöltés https://commons.wikimedia.org/
* Hallgatás/letöltés https://freemusicarchive.org/
* Letöltés http://ccmixter.org/
* Hallgatás/letöltés https://archive.org/details/audio_music
* Hallgatás/letöltés http://magnatune.com/info/whynotevil (ingyen is használható, de etikusan felkér az adakozásra)

### Főleg ingyenes tartalmak

* Hallgatás/letöltés https://soundcloud.com/
* Hallgatás/letöltés https://www.mixcloud.com/
* Rádiózás https://www.jango.com/ igény szerinti hallgatáshoz csak külsős YouTube linkeket biztosítanak
* Rádiózás MindigTV UHF Petőfi rádió esti rontom bontom dj set (bár az utóbbi években sokat változott a stílusuk), online egy kicsit sok Javascriptet futtat
* Rádiózás https://tilos.hu/ vagy FM, letöltés https://tilos.hu/mixes/dj
* Rádiózás https://www.di.fm/ 30 percet regisztráció nélkül is lejátszik, utána ingyenes regisztrációt vár el
* Hallgatás/letöltés https://bandcamp.com/tag/free-download A hallgatás mindig ingyenes, de van amelyik szám letöltése fizetős, amelyik ingyenes, azt regisztráció nélkül is le lehet tölteni
* Hallgatás https://bleep.com/ a letöltés mindig fizetős

### Vegyes demoscene és retro chiptune

* Rádiózás/néha letöltés https://www.scenestream.net/demovibes/streams/
* Letöltés https://files.scene.org/
* Letöltés https://modarchive.org/
* Letöltés ftp://ftp.hornet.org/pub/demos/music/contests/
* Letöltés http://www.chiptune.com/
* Letöltés https://chipmusic.org/
* Letöltés http://www.8bitpeoples.com/
* Letöltés http://www.traxinspace.com/ (TODO: regisztráció szükséges?)
* Rádiózás http://www.kohina.com/
* Hallgatás/letöltés https://www.pouet.net/ "videoklipeket" is ad, bár néha YouTube-on hosztolják, akad HTTP/FTP vagy egyéb letöltés is, de akár otthon is generálható

### Ingyenes háttérzenék

* Hallgatás/letöltés https://incompetech.com/

## Pingback

Ha átnevezésre vagy áthelyezésre kerül ez a fájl, az összes közvetlen hivatkozást frissíteni kell:

* https://hup.hu/comment/2568145#comment-2568145
